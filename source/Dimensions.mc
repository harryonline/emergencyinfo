
using Toybox.System;
using Toybox.Math;

//! Return the width at certain Y position
function getWidthAt(posY) {
	var settings = System.getDeviceSettings();

	if( settings.screenShape == System.SCREEN_SHAPE_RECTANGLE ) {
		return settings.screenWidth;
	}
	var r = settings.screenWidth / 2;
	var dY = posY - settings.screenHeight / 2;

	if( dY.abs() >= r ) {
		return 0;
	}
	var w = 2 * Math.sqrt( r * r - dY * dY);
	return w;
}
