using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.System as System;

var appVersion = "1.5.0";
var fields;
var labels;

class EmergencyInfoApp extends App.AppBase {

  function initialize() {
    AppBase.initialize();
    App.getApp().setProperty("appVersion", appVersion);
    fields = ["name", "dob", "phone", "emergency", "other"];
    labels = ["label_title", "label_dob", "label_phone", "label_emergency"];
  }

  //! Return the initial view of your application here
  function getInitialView() {
    return [ new EmergencyInfoView(), new EmergencyInfoDelegate() ];
  }

  function getGlanceView() {
    return [ new GlanceView()];
  }
}

function getProperty(key) {
  var value = App.getApp().getProperty(key);
  if( value == null ) {
    return "";
  }
  return value.toString();
}
