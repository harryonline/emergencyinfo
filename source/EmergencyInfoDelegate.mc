using Toybox.WatchUi as Ui;
using Toybox.System;
using Toybox.Application as App;

var settingField;
var symbols;

class EmergencyInfoDelegate extends Ui.BehaviorDelegate {

  function initialize() {
    BehaviorDelegate.initialize();
    symbols = Ui has :TextPicker ? [:name, :dob, :phone, :emergency, :other, :about] : [:about];
  }

  function onMenu() {
    var menu = new Ui.Menu();
    for( var i = 0; i < symbols.size(); i++ ) {
      menu.addItem( Rez.Strings[symbols[i]], symbols[i]);
    }
    Ui.pushView( menu, new SettingsMenuDelegate(), SLIDE_IMMEDIATE );
    return true;
  }
}

class SettingsMenuDelegate extends Ui.MenuInputDelegate {

  function initialize() {
    MenuInputDelegate.initialize();
  }

  function onMenuItem(item) {
    if( item == :about ) {
      Ui.pushView(new AboutView(), new AboutDelegate(), Ui.SLIDE_IMMEDIATE );
      return;
    }
    for ( var i = 0; i < symbols.size(); i++ ) {
      if( item == symbols[i] ) {
        settingField = fields[i];
        Ui.pushView(new Ui.TextPicker(getProperty(settingField)), new KeyboardListener(), Ui.SLIDE_IMMEDIATE );
      }
    }
  }
}

class KeyboardListener extends Ui.TextPickerDelegate {

  function initialize() {
    TextPickerDelegate.initialize();
  }

  function trim(str) {
    var len = str.length();
    while( str.substring(0,1).equals(" ")) {
      str = str.substring(1, len);
    }
    while( str.substring(len-1,len).equals(" ")) {
      str = str.substring(0, len-1);
    }
    return str;
  }

  function onTextEntered(text, changed) {
    if( changed ) {
      var trimmed = trim(text);
      App.getApp().setProperty(settingField, trimmed);
    }
  }
}

