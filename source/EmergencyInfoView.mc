using Toybox.Application as App;
using Toybox.Graphics as Gfx;
using Toybox.WatchUi as Ui;
using Toybox.Timer;
using Toybox.System;

var scrollTimer = new Timer.Timer();

//! Idea: create list of drawables in OnLayout, display in onUpdate, see also :
//! https://forums.garmin.com/showthread.php?183397-Can-someone-please-demonstrate-a-working-example-of-Ui-animate&highlight=animate+text

class EmergencyInfoView extends Ui.View {

  var scrollCounter = -1;

  var font_xtiny;
  var font_tiny;
  var useSmallFont = false;
  var nameWidth = 0;
  var otherWidth = 0;
  var firstDraw = true;

  function initialize() {
    View.initialize();
  }

  function getNameWidth(y) {
    if (nameWidth == 0) {
      nameWidth = getWidthAt(y);
    }
    return nameWidth;
  }

  function getOtherWidth(y) {
    if (otherWidth == 0) {
      otherWidth = getWidthAt(y);
    }
    return otherWidth;
  }

  //! Load your resources here
  function onLayout(dc) {
    var i;

    //! Need to use smaller fonts? Check device number
    var settings = System.getDeviceSettings();
    var partNumber = settings.partNumber;
    var screenHeight = settings.screenHeight;
    System.println("Part number: " + partNumber);
    if (!partNumber.equals("006-B1988-00")) {  //! Epix
      if (screenHeight == 240) {
        useSmallFont = true;
      } else {
        var smallFontPartsNumbers = [ "006-B2544-00", "006-B2432-00", "006-B4115-00"];  //! Fenix5s, chronos, venu sq2
        useSmallFont = smallFontPartsNumbers.indexOf(partNumber) != -1;
      }
    }
    setLayout(Rez.Layouts.MainLayout(dc));
    if (useSmallFont) {
      font_xtiny = Ui.loadResource(Rez.Fonts.id_font_xtiny);
      font_tiny = Ui.loadResource(Rez.Fonts.id_font_tiny);
      
    }
    for( i = 0; i < labels.size(); i++ ) {
      View.findDrawableById(labels[i]).setText(getProperty(labels[i]));
      if (useSmallFont) {
        View.findDrawableById(labels[i]).setFont(font_xtiny);
      }
    }
    for( i = 0; i < fields.size(); i++ ) {
      var field = fields[i];
      var text = getProperty(field);
      var drawable = View.findDrawableById(field);
      if( !field.equals("name") && !field.equals("other")) {
        if (useSmallFont) {
          drawable.setFont(font_tiny);
        }
        drawable.setText(text);
      }
    }
  }

  function onUpdate(dc) {
    var i;
    View.onUpdate(dc);
    for( i = 0; i < fields.size(); i++ ) {
      var field = fields[i];
      var text = getProperty(field);
      var drawable = View.findDrawableById(field);
      var element = createText(drawable, field);
      if( field.equals("name")) {
        fitDraw(dc, element, text, Gfx.FONT_MEDIUM, field);
      } else if( field.equals("other")) {
        if (moreLines(element.locY)) {
          writeLines(dc, text, Gfx.FONT_SMALL, element.locY, 10, method(:write));
        } else {
          fitDraw(dc, element, text, Gfx.FONT_TINY, field);
        }
      } else if (firstDraw && !useSmallFont) {
        var spc = getWidthAt(drawable.locY + drawable.height / 2) - drawable.locX - drawable.width;
        if (spc < 0) {
          var label = View.findDrawableById("label_" + field);
          drawable.setLocation(drawable.locX, label.locY);
          drawable.setFont(Gfx.FONT_XTINY);
        }
      }
    }
    firstDraw = false;
  }

  function scrollCallback() {
    scrollCounter ++;
    Ui.requestUpdate();
  }

  function fitDraw(dc, element, text, font, field) {
    var viewWidth = getViewWidth(dc, field, element.locY, font) - 5;
    var x0 = (dc.getWidth() - viewWidth) / 2;
    if (viewWidth < 120) {
      x0 = 4;
  }
    var scrollStep = 10;
    var textWidth = dc.getTextWidthInPixels(text, font);
    if ( textWidth > viewWidth && font > 0 ) {
      font --;
      element.setFont(font);
      textWidth = dc.getTextWidthInPixels(text, font);
    }
    if( textWidth > viewWidth ) {
      // Need to scroll
      element.setLocation(0, element.locY);
      element.setJustification(Gfx.TEXT_JUSTIFY_LEFT);
      var excess = textWidth - viewWidth;
      var maxSteps = (1.0 * excess/scrollStep).toNumber() + 2;
      if( scrollCounter == -1 ) {
        scrollCounter = 0;
        scrollTimer.start(method(:scrollCallback), 1000, true);
      }
      element.setLocation(x0 - ((scrollCounter % maxSteps) * scrollStep), element.locY);
    }
    element.draw(dc);
  }

  function write( dc, text, font, locY ) {
    var linePadding = 0.1;
    var height = dc.getFontHeight(font);
    //dc.drawText(dc.getWidth()/2, locY, font, text, Gfx.TEXT_JUSTIFY_CENTER);
    dc.drawText(5, locY + height*linePadding, font, text, Gfx.TEXT_JUSTIFY_LEFT);
    return locY + height * (1 + 2 * linePadding );
  }

  function moreLines(locY) {
    var settings = System.getDeviceSettings();
    return settings.screenShape == System.SCREEN_SHAPE_RECTANGLE  && settings.screenHeight - locY > 40;
  }

  function getViewWidth(dc, field, locY, font) {
    var settings = System.getDeviceSettings();
    var result = dc.getWidth() - 10;
    if (settings.screenShape == System.SCREEN_SHAPE_RECTANGLE) {
      return result;
    }
    if (field.equals("name" )) {
      return getNameWidth(locY);
    }
    if (field.equals("other" )) {
      var y = locY + dc.getFontAscent(font);
      return getOtherWidth(y);
    }
    return result;
  }

  function createText(element, field) {
    var font = Gfx.FONT_TINY;
    var justification = Gfx.TEXT_JUSTIFY_LEFT;
    if( field.equals("name" )) {
      font = Gfx.FONT_MEDIUM;
      justification = Gfx.TEXT_JUSTIFY_CENTER;
    }
    if( field.equals("other" )) {
      justification = element.locX < 20 ? Gfx.TEXT_JUSTIFY_LEFT : Gfx.TEXT_JUSTIFY_CENTER;
    }
    return new Ui.Text({
      :text => getProperty(field),
      :color => Gfx.COLOR_WHITE,
      :backgroundColor => Gfx.COLOR_TRANSPARENT,
      :font => font,
      :justification => justification,
      :locX => element.locX,
      :locY => element.locY
    });
  }
}
