
function writeLines(dc, text, font, locY, padding, callback) {
  var parts = ["", text];
  while( parts.size() == 2 ) {
    var width = dc.getWidth() - padding;
    parts = lineSplit(dc, parts[1], font, width);
    locY = callback.invoke(dc, parts[0], font, locY) ;
  }
}

function lineSplit(dc, text, font, width) {
  var os = 0;
  var parts = wordSplit(text, os);
  var count = 0;
  while (parts.size() == 2 && count < 10) {
    var newParts = wordSplit(text, parts[0].length()+1);
    if (dc.getTextWidthInPixels(newParts[0], font) > width ) {
      break;
    }
    count ++;
    parts = newParts;
  }
  return parts;
}

/**
*  Splits the subject into max. 2 parts on the first space after start
**/
function wordSplit(subject, start) {
  var len = subject.length();
  var substr = subject.substring(start, len);
  var ptr = substr.find(" ");
  return ptr == null ? [subject] : [subject.substring(0, ptr+start), subject.substring( ptr+start+1, len)];
}
